package com.cpl_cursos.java.spring1.Modelos;

public class Usuario {
    private String nombre;
    private String apellidos;
    private String emilio;

    public Usuario() {
    }

    public Usuario(String nombre, String apellidos, String emilio) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.emilio = emilio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmilio() {
        return emilio;
    }

    public void setEmilio(String emilio) {
        this.emilio = emilio;
    }
}
