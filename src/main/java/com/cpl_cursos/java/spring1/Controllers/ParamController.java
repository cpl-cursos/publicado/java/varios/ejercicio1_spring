package com.cpl_cursos.java.spring1.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/params")
public class ParamController {

    @GetMapping({"/",""})
    public String index(Model modelo) {
        modelo.addAttribute("titulo","Ejemplo de paso de parámetros en la URL.");
        modelo.addAttribute("titulopest","Ejemplo param.");
        return "params/index";
    }

    @GetMapping("/cadena/{cad1}")
    public String var1(@PathVariable String cad1, Model modelo) {
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo parámetros desde la ruta con @(PathVariable)");
        modelo.addAttribute("parametro", "El parámetro recibido es: " + cad1);
        return "params/ver";
    }

    @GetMapping("/cadena/{cad1}/{nro}")
    public String var1(@PathVariable String cad1, @PathVariable Integer nro, Model modelo) {
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo parámetros desde la ruta con @(PathVariable)");
        modelo.addAttribute("parametro", "El parámetro 1 recibido es: '" + cad1 + "' y el parámetro 2 es: '" + nro + "'");
        return "params/ver";
    }

    @GetMapping("/mixparams")
    public String param(@RequestParam String param1, Model modelo) {
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo parámetros desde la ruta con @(RequestParam)");
        modelo.addAttribute("parametro", "El parámetro recibido es: " + param1);
        return "params/mix";
    }

    @GetMapping("/mixparams2")
    public String param2(@RequestParam String param1, @RequestParam(required = false) Integer nro, Model modelo) {
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo 2 parámetros desde la ruta con @(RequestParam)");
        modelo.addAttribute("parametro", "Los parámetros son : '" + param1 + "' y el nro.: '" + nro + "'");
        return "params/mix";
    }

    @GetMapping("/mixparamsreq")
    public String paramreq (HttpServletRequest req, Model modelo) {
        String param1 = req.getParameter("param1");
        Integer nro = null;
        try {
            nro = Integer.parseInt(req.getParameter("nro"));
        } catch (NumberFormatException e) {
            nro = 0;
        }
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo 2 parámetros desde la ruta con @(RequestParam)");
        modelo.addAttribute("parametro", "Los parámetros son : '" + param1 + "' y el nro.: '" + nro + "'");
        return "params/mix";
    }

    @GetMapping("/listaparams")
    public String listaparam (@RequestParam Map<String,String> allParams, Model modelo) {
        modelo.addAttribute("titulopest","Ejemplo param.");
        modelo.addAttribute("titulo", "Recibo n parámetros desde la ruta con @(RequestParam)");
        modelo.addAttribute("parametros", allParams);
        return "params/listaparams";
    }
}
