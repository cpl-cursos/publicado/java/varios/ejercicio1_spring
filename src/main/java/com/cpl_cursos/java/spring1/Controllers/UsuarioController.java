package com.cpl_cursos.java.spring1.Controllers;

import com.cpl_cursos.java.spring1.Modelos.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/usu")
public class UsuarioController {

    @GetMapping("/perfil")
    public String perfil(Model modelo) {
        Usuario usu = new Usuario();
        usu.setNombre("Carlos");
        usu.setApellidos("Ponce de León");
        //usu.setEmilio("carlos@centropyme.com");
        modelo.addAttribute("titulo", "Perfil del usuario " + usu.getNombre());
        modelo.addAttribute("usuario", usu);
        return "perfil";
    }

    @GetMapping("/lista")
    public String listar(Model modelo) {
        modelo.addAttribute("titulo", "Lista de usuarios");
        return "lista_usuarios";
    }

    @ModelAttribute("listausu")
    public List<Usuario> llenarListaUsu() {
        List<Usuario> usuarios = Arrays.asList(
                new Usuario("Carlos", "Ponce de León", "carlos@google.com"),
                new Usuario("Pepe", "González", "pepe@google.com"),
                new Usuario("Rosa", "García", "rosa@google.com"),
                new Usuario("Ana", "López", "ana@google.com"),
                new Usuario("Luis", "Perez", "luis@google.com")
        );
        return usuarios;
    }
}
