package com.cpl_cursos.java.spring1.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class IndexController {

    @GetMapping("/inicio")
    public String inicio(Model modelo) {
        modelo.addAttribute("titulo", "Hola desde el ejercicio 1 con datos, que ahora se llamará ejercicio 2 en el commit.");
        return "index";
    }

    @GetMapping("/")
    public String inicio2(ModelMap modmap) {
        modmap.addAttribute("titulo", "Hola desde el ejercicio 3 con datos pasados con un ModelMap.");
        return "index";
    }

    @GetMapping("/modo3")
    public String inicio3(Map<String, Object> map) {
        map.put("titulo", "Hola desde el ejercicio 4 con datos pasados con un Map.");
        return "index";
    }

    @GetMapping("/modo4")
    public ModelAndView modo4(ModelAndView manv) {
        manv.addObject("titulo", "Hola desde el ejercicio 5 con datos pasados con un ModelAndView.");
        manv.setViewName("index");
        return manv;
    }
}
